export default function testReducer (state = [], action) {
  switch (action.type) {
    case 'TEST_IT':
      return action.payload
    default:
      return state
  }
}
