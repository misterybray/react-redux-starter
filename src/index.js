import React from 'react'
import { render } from 'react-dom'

import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'

import createHistory from 'history/createBrowserHistory'
import { Route, Router } from 'react-router'

import { routerReducer, routerMiddleware } from 'react-router-redux'

import thunk from 'redux-thunk'

import reducers from './reducers'

import './style/index.css' // dont know yet
import App from './components/App'
import registerServiceWorker from './utils/registerServiceWorker'

const history = createHistory()

const middleware = routerMiddleware(history)

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
  combineReducers({
    ...reducers,
    router: routerReducer
  }),
  composeEnhancers(
    applyMiddleware(
      middleware,
      thunk
    )
  )
)

render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App}></Route>
    </Router>
  </Provider>,
  document.getElementById('root')
)

registerServiceWorker()
