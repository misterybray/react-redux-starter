import React, { Component } from 'react'
import { Route, Switch } from 'react-router'
import { connect } from 'react-redux'

import Root from './Root'

class App extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route path="/" component={Root} exact />
        </Switch>
      </div>
    )
  }
}

export default connect()(App)
